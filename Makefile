# Folder structure
SRC := ./src
INC := ./inc
OBJ := ./obj
LIB := ./lib
BIN	:= ./bin

# Compiler
COMPILER := g++
CUDA_COMPILER := nvcc

# CUDA path
CUDA_PATH := /opt/cuda
CUDA_EXEC := optirun nvprof

# Libraries
MY_LIB := metcomp
CUDA_LIBS := -L$(CUDA_PATH)/lib64 -lcudart -lcurand

# Common includes and paths and libraries
INCLUDES  := -I$(INC) -I$(CUDA_PATH)/include
LIBRARIES := -L$(LIB) -l$(MY_LIB)

################################################################################

LIB_OBJECTS = $(OBJ)/metcomp.o $(OBJ)/timer.o $(OBJ)/dynsys.o
CUDA_OBJECTS = $(LIB_OBJECTS) $(OBJ)/metcomp_cuda.o $(OBJ)/vector_cuda.o $(OBJ)/potts_cuda.o

APPLICATION = main
CUDA_APPLICATION = $(APPLICATION)_cuda
APPLICATION_OBJECT = $(OBJ)/$(APPLICATION).o

################################################################################

# CUDA objects rule
$(OBJ)/%_cuda.o: $(SRC)/%.cu
	mkdir -p $(OBJ)
	$(CUDA_COMPILER) $< $(INCLUDES) -c -o $@ -O3

# C++ objects rule
$(OBJ)/%.o: $(SRC)/%.cpp
	mkdir -p $(OBJ)
	$(COMPILER) $< $(INCLUDES) -c -o $@ -O3

################################################################################

# Regular compilation
all: lib app

lib: $(LIB_OBJECTS)
	mkdir -p $(LIB)
	ar rcs $(LIB)/lib$(MY_LIB).a $(LIB_OBJECTS)

app: $(APPLICATION_OBJECT)
	mkdir -p $(BIN)
	$(COMPILER) $< $(INCLUDES) $(LIBRARIES) -o $(BIN)/$(APPLICATION)

run: clean all
	$(BIN)/$(APPLICATION)

################################################################################

# CUDA compilation
cuda_build: $(CUDA_OBJECTS) $(APPLICATION_OBJECT)
	mkdir -p $(BIN)
	$(CUDA_COMPILER) $+ $(INCLUDES) $(CUDA_LIBS) -o $(BIN)/$(CUDA_APPLICATION)

cuda_run: clean cuda_build
	$(CUDA_EXEC) $(BIN)/$(CUDA_APPLICATION)

################################################################################

# Clean folder
clean:
	rm -rf $(BIN)/* $(LIB)/* $(OBJ)/*
