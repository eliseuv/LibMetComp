data1 = './dat/Potts2D/shorttime/Potts2D_32x32_q=2_J=1_sig=0_T=8.3_runs=2048_t=64_Magnet.dat'
data2 = './dat/Potts2D/shorttime/Potts2D_32x32_q=2_J=1_sig=0_T=8.4_runs=2048_t=64_Magnet.dat'
data3 = './dat/Potts2D/shorttime/Potts2D_32x32_q=2_J=1_sig=0_T=8.5_runs=2048_t=64_Magnet.dat'
data4 = './dat/Potts2D/shorttime/Potts2D_32x32_q=2_J=1_sig=0_T=8.55_runs=2048_t=64_Magnet.dat'
data5 = './dat/Potts2D/shorttime/Potts2D_32x32_q=2_J=1_sig=0_T=8.6_runs=2048_t=64_Magnet.dat'
data6 = './dat/Potts2D/shorttime/Potts2D_32x32_q=2_J=1_sig=0_T=8.7_runs=2048_t=64_Magnet.dat'
set title 'Potts2D 32x32 J=1 q=2 sigma=0 2048 runs 64 MC steps'
#set xlabel 't'
#set ylabel 'm'
set logscale
set xlabel 'log(t)'
set ylabel 'log(m)'
a = -1;
b = 5;
f(x) = a*x+b;
fit f(x) data4 using (log($1)):(log($2)) via a,b;
plot data1 t 'T = 8.3', data2 t 'T = 8.4', data3 t 'T = 8.5', data4 t 'T = 8.55', data5 t 'T = 8.6', data6 t 'T = 8.7', [x=1:64] exp(f(log(x))) t 'fit T = 8.55';
