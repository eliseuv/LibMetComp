data1 = './dat/Potts2D/equilibrium/Potts2D_32x32_q=2_J=1_sig=10_teq=1024_tcalc=4096_Suscept.dat'
set title 'Potts2D: 32x32, J = 1, q = 2, sigma = 0, t_{eq}= 1024, t_{calc} = 4096'
set xlabel 'T'
set ylabel '{/Symbol c}(T)'
set xtics 0.1
unset key
plot data1 w l;
