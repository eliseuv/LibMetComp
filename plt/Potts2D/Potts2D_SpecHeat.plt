data1 = './dat/Potts2D/equilibrium/Potts2D_32x32_q=2_J=1_sig=10_teq=256_tcalc=4096_SpecHeat.dat'
set title 'Potts2D: 32x32, J = 1, q = 2, sigma = 10, t_{eq}= 256, t_{calc} = 4096'
set xlabel 'Temperature'
set ylabel 'Specific Heat'
set xtics 0.1
unset key
plot data1;
