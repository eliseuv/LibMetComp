#pragma once

/*************************/
/*    Dynamical System   */
/*************************/

#include "metcomp.h"

#include <vector>
#include <functional>
#include <string>

#define Vector std::vector

template <typename real>
class DynSys
{
private:
  unint _N;
  real _t;
  bool _p;
  Vector<real> _x0;
  Vector<real> _x1;
  Vector<std::function<real(Vector<real>)> > _S;

  void _rk4(void);

public:

  // Constructor
  DynSys (unint N);

  // Destructor
  // virtual ~DynSys ();

  // Set system state
  void set_state(unint n, real xn);
  void set_state(Vector<real> x);

  // Set function in the system
  void set_function(unint n, std::function<real(Vector<real>)> f);

  // Set system
  void set_system(Vector<std::function<real(Vector<real>)>> S);

  // Map evolve (Discrete-time)
  void map_evolve(void);

  // Euler integration
  void euler(real dt);
  void euler(unint steps, real dt);

  // Runge-Kutte 4th order
  void rk4(real dt);

  // Get state
  real get_state(unint n);

  // Print
  void print(void);

};
