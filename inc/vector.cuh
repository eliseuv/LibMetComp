#pragma once

/**************************/
/*    CUDA Vector Class   */
/**************************/

// #define _CHECK_BOUNDS_

#include "metcomp.h"

template <class T>
class Vector
{
private:
  // Vector size
	unint _N;
  // CUDA Thread blocks
  unint _blockSize;
  unint _numBlocks;

public:

  // Pointer
  T * _p;

  // Make type available externally
  typedef T type;

  // Default constructor
	Vector();
  // Zero-based vector
	explicit Vector(unint N);
  // Initialize to constant value
	Vector(unint N, const T a);
	// Vector(int n, const T *a);	// Initialize to array
	// Vector(const Vector &rhs);	// Copy constructor
	// Vector & operator=(const Vector &rhs);	//assignment

  // Access i'th element
	T & operator[](const unint i);

  // Sum of elements
  T sum(void);

  // Multiply by a constant
  void mult(T & a);
  // Sum with another vector
  void add(const Vector & v);

	// inline const T & operator[](const int i) const;
	// inline int size() const;
	// void resize(int newn); // resize (contents not preserved)
	// void assign(int newn, const T &a); // resize and assign a constant value
	~Vector();
};
