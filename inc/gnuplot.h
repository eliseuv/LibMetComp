#pragma once

/**************************/
/*    gnuplot Interface   */
/**************************/

#include <iostream>
#include <string>

class Gnuplot
{
private:

  FILE* pipe;

public:

  // Constructors
  Gnuplot(void);
  Gnuplot(const std::string &command);
  // Destructor
  ~Gnuplot(void);
  // Type command directly
  void operator()(const std::string &command);
  // Enter pair of numbers
  template <typename real>
  void point2D(real v0, real v1);
  // Enter trio of numbers
  template <typename real>
  void point3D(real v0, real v1, real v2);

};

// Constructors
Gnuplot::Gnuplot(void)
{
  pipe = popen("gnuplot -persist", "w");
  if (!pipe)
    std::cerr << "Gnuplot not found!" << std::endl;
}

Gnuplot::Gnuplot(const std::string &command)
  : Gnuplot()
{ this->operator()(command); }

// Destructor
Gnuplot::~Gnuplot()
{
  fprintf(pipe, "exit\n");
  pclose(pipe);
}

// Type command directly
void Gnuplot::operator()(const std::string &command)
{
  fprintf(pipe, "%s\n", command.c_str());
  fflush(pipe);
}

// Enter pair of numbers
template <typename real>
void Gnuplot::point2D(real v0, real v1)
{
  fprintf(pipe, "%f\t%f\n", v0, v1);
  fflush(pipe);
}

// Enter triplet of numbers
template <typename real>
void Gnuplot::point3D(real v0, real v1, real v2)
{
  fprintf(pipe, "%f\t%f\t%f\n", v0, v1, v2);
  fflush(pipe);
}
