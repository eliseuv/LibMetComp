#pragma once

/********************/
/*    Potts Model   */
/********************/

#include <vector>
#include <random>
#include <string>

#include <curand_kernel.h>

#include "metcomp.h"

/**
 * 2D Potts Model With Long Range Interactions
 *
 * Hamiltonian:
 *  H = -J \sum_{i,j} \frac{s_i s_j}{|i - j|^{2+\sigma}}
 *
 *  i, j    : Positions in a 2D lattice
 *  s_i     : Spin state at position i (s_i \in {0, 1, ..., q-1})
 *  J       : Coupling constant
 *  |.|     : Euclidean norm
 *  \sigma  : Dependance on the distance
 *
 * Template:
 *  real = float, double
 *
 * Members:
 *
 * _t             : Keeps track of the time (evolution of the system)
 * _runs          : Keeps track of the number of accumulated runs
 * _lx            : x dimension of the lattice
 * _ly            : y dimension of the lattice
 * _N             : Total number of spins in the Lattice
 * _q             : Number of possible states for each spin
 * *_lattice      : Pointer to the host copy of the lattice
 * *_gpu_lattice  : Pointer to the device copy of the lattice
 * _T             : Temperature
 * _J             : Coupling constant (Energy)
 * _BJ            : = \beta J = \frac{J}{k_B T} (Adimensional)
 * _sig           : Range parameter \sigma \in (0, 1) (Adimensional)
 * _ssig          : = \frac{2 + \sigma}{2} (Adimensional)
 * _E[0 ... _t]   : Vector holding the energy values of the system
 * _M[0 ... _t]   : Vector holding the magnetization values of the system
 *
 * Members with _cpu ending do basically the same thing but on the host
 *
 * TODO: Filo IO interface.
 *
 */
template <typename real>
class Potts2D
{
private:
  // Time
  unint _t;
  unint _runs;
  // Lattice
  unint _lx, _ly, _N, _q;
  unint *_lattice;
  unint *_gpu_lattice;
  // Temperature and interactions
  real _T, _J, _BJ, _sig, _ssig;
  // System data
  real *_E;
  real *_M;

  // Initialize Lattice
  void _Lattice_fill(const bool T0inf, const unint q0);

  // Magnetization
  real _Magnet(void);
  real _Magnet_cpu(void);

  // Long Range Hamiltonian
  real _H_LongRange(void);
  real _H_LongRange_cpu(void);
  // Local Hamiltonian
  // real _H_LongRange_local(unint ix, unint iy, unint s);
  real _H_LongRange_local_cpu(unint ix, unint iy, unint s);

public:

  // Monte Carlo
  // void MC_Metropolis(const unint steps);
  // void MC_Metropolis(const unint runs, const unint steps);
  void MC_Metropolis_cpu(const unint steps);
  void MC_Metropolis_cpu(const unint runs, const unint steps);

  // Magnetic Susceptibility
  // real suscept(const unint eq_steps);
  real suscept_cpu(const unint eq_steps);

  // Specific Heat
  real spec_heat(const unint eq_steps);
  real spec_heat_cpu(const unint eq_steps);

  // Constructor
  Potts2D(const unint lx, const unint ly, const unint q, const real T, const real J, const real sig, const bool T0inf, const unint s0);
  // Destructor
  ~Potts2D();

  // Reset
  void reset(const real T, const real J, const real sig, const bool T0inf, const unint s0);

  // Getters
  real energy(void);
  real magnet(void);

  // Print
  void print_grid(void);
  void print(void);
  // Plot
  void plot_magnet(void);
  void plot_energy(void);

  // File IO
  void write_data(void);
};
