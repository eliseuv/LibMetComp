#pragma once

/***************/
/*    Timer    */
/***************/

#include "metcomp.h"

#include <chrono>
#include <string>

template <typename real>
class Timer {

  typedef std::chrono::time_point<std::chrono::high_resolution_clock> hdtime;
  typedef std::chrono::duration<real> duration;

  hdtime _start;
  hdtime _end;
  duration _elapsed;
  real _value;

public:
  // Constructors
  Timer(void);
  // Start/Stop
  void start(void);
  void stop(void);
  // Return elapsed time
  real val(void);
  // Print elapsed time
  void print(std::string description);

};
