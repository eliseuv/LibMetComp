#pragma once

/******************************************/
/*    Computational Methods for Physics   */
/*            CUDA Capabilities           */
/******************************************/

#include <cstdio>
#include <cuda_runtime.h>

// CUDA query device
void CUDA_queryDevices(void);
