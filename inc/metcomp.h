#pragma once

/******************************************/
/*    Computational Methods for Physics   */
/******************************************/

#include <cstdio>
#include <iostream>
#include <fstream>

// #define _ERROR_CLASS_

/*  Exception Handling

try {
  somebadroutine();
}
catch(Error s) {
  Catch(s);
}

*/

// #ifdef _ERROR_CLASS_
// #define throw(message){printf("ERROR: %s\n     in file %s at line %d\n",       \
//                               message, __FILE__, __LINE__);                    \
//                        throw(1); }
// #else
// struct Error {
//   char *message;
//   char *file;
//   int line;
//   Error(char *m, char *f, int l) : message(m), file(f), line(l) {}
// };
// #define throw(message) throw(Error(message, __FILE__, __LINE__));
// void Catch(Error err) {
//   printf("ERROR: %s\n     in file %s at line %d\n", err.message, err.file,
//          err.line);
//   exit(1);
// }
// #endif

/*  Macro-like inline functions

  SWAP(a, b): a <-> b
  SQR(a) = a*a
  MAX(a, b)
  MIN(a, b)
  SIGN(a, b)

*/

template <typename T> inline void SWAP(T &a, T &b) {
  T dum = a;
  a = b;
  b = dum;
}

template <typename T> inline T SQR(const T a) { return a * a; }

template <typename T, typename T1, typename T2>
inline const T &MAX(const T1 &a, const T2 &b) {
  return b > a ? (b) : (a);
}

template <typename T, typename T1, typename T2>
inline const T &MIN(const T1 &a, const T2 &b) {
  return b < a ? (b) : (a);
}

template <typename T, typename T1, typename T2>
inline T SIGN(const T1 &a, const T2 &b) {
  return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);
}

/*  New type names

  unint : Natural Numbers

*/

typedef unsigned int unint;

/* Template explicit instatiations

  real : float, double

*/

#define INSTANTIATE_REAL(CLASS) \
  template class CLASS<float>; \
  template class CLASS<double>;
