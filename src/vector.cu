#include "vector.cuh"

/**************************/
/*    CUDA Vector Class   */
/**************************/

// Default constructor
template <class T>
Vector<T>::Vector()
  : _N(0), _p(NULL)
  {}

// Zero-based vector
template <class T>
Vector<T>::Vector(unint N)
  : _N(N)
  // : _N(N), _p(N>0 ? new T[N] : NULL)
  {
    _blockSize = 256;
    _numBlocks = (N + _blockSize - 1) / _blockSize;
    std::cout << "nBlocks = " << _numBlocks << ", blockSize = "<< _blockSize << '\n';
    cudaMallocManaged(&_p, N*sizeof(T));
  }

// Initialize to constant value
template <class T>
__global__ void assign_kernel(unint N, T *p, const T a)
{
  unint index = blockIdx.x * blockDim.x + threadIdx.x;
  unint stride = blockDim.x * gridDim.x;
  for (unint i = index; i < N; i += stride)
    p[i] = a;
}

template <class T>
Vector<T>::Vector(unint N, const T a)
  : Vector(N)
{
  assign_kernel<<<_numBlocks, _blockSize>>>(N, _p, a);
  cudaDeviceSynchronize();
}

// template <class T>
// Vector<T>::Vector(int n, const T *a) : nn(n), v(n>0 ? new T[n] : NULL)
// {
// 	for(int i=0; i<n; i++) v[i] = *a++;
// }
//
// template <class T>
// Vector<T>::Vector(const Vector<T> &rhs) : nn(rhs.nn), v(nn>0 ? new T[nn] : NULL)
// {
// 	for(int i=0; i<nn; i++) v[i] = rhs[i];
// }
//
// template <class T>
// Vector<T> & Vector<T>::operator=(const Vector<T> &rhs)
// // postcondition: normal assignment via copying has been performed;
// //		if vector and rhs were different sizes, vector
// //		has been resized to match the size of rhs
// {
// 	if (this != &rhs)
// 	{
// 		if (nn != rhs.nn) {
// 			if (v != NULL) delete [] (v);
// 			nn=rhs.nn;
// 			v= nn>0 ? new T[nn] : NULL;
// 		}
// 		for (int i=0; i<nn; i++)
// 			v[i]=rhs[i];
// 	}
// 	return *this;
// }

// Access i'th element
template <class T>
T & Vector<T>::operator[](const unint i)
{
#ifdef _CHECK_BOUNDS_
if (i<0 || i>=_N) {
	throw("Vector subscript out of bounds");
}
#endif
	return _p[i];
}

// Sum of elements
template <class T>
T Vector<T>::sum(void)
{
  T S = 0;
  for (unint i = 0; i < _N; i++)
    S += _p[i];
  return S;
}

// Multiply by a constant
template <class T>
__global__ void mult_kernel(unint N, T *p, T *a)
{
  unint index = blockIdx.x * blockDim.x + threadIdx.x;
  unint stride = blockDim.x * gridDim.x;
  for (unint i = index; i < N; i += stride)
    p[i] = a[0]*p[i];
}
template <class T>
void Vector<T>::mult(T & a)
{
  T * ap;
  cudaMallocManaged(&ap, sizeof(T));
  *ap = a;
  mult_kernel<<<_numBlocks, _blockSize>>>(_N, _p, ap);
  cudaFree(ap);
  cudaDeviceSynchronize();
}

// Sum with another vector
template <class T>
__global__ void add_kernel(unint N, T *p1, T *p2)
{
  unint index = blockIdx.x * blockDim.x + threadIdx.x;
  unint stride = blockDim.x * gridDim.x;
  for (unint i = index; i < N; i += stride)
    p1[i] = p1[i] + p2[i];
}
template <class T>
void Vector<T>::add(const Vector & v2)
{
  add_kernel<<<_numBlocks, _blockSize>>>(_N, _p, v2._p);
  cudaDeviceSynchronize();
}

// template <class T>
// inline const T & Vector<T>::operator[](const int i) const	//subscripting
// {
// #ifdef _CHECKBOUNDS_
// if (i<0 || i>=nn) {
// 	throw("Vector subscript out of bounds");
// }
// #endif
// 	return v[i];
// }
//
// template <class T>
// inline int Vector<T>::size() const
// {
// 	return nn;
// }
//
// template <class T>
// void Vector<T>::resize(int newn)
// {
// 	if (newn != nn) {
// 		if (v != NULL) delete[] (v);
// 		nn = newn;
// 		v = nn > 0 ? new T[nn] : NULL;
// 	}
// }
//
// template <class T>
// void Vector<T>::assign(int newn, const T& a)
// {
// 	if (newn != nn) {
// 		if (v != NULL) delete[] (v);
// 		nn = newn;
// 		v = nn > 0 ? new T[nn] : NULL;
// 	}
// 	for (int i=0;i<nn;i++) v[i] = a;
// }

template <class T>
Vector<T>::~Vector()
{
	if (_p != NULL) cudaFree(_p);
}

// Explicit instatiations
INSTANTIATE_REAL(Vector)
