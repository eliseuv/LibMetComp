#include <iostream>
#include <cmath>
#include <cstdio>

#include "metcomp.h"
#include "timer.h"
#include "dynsys.h"
#include "gnuplot.h"

int main() {

  // // Tinkerbell
  // double a = 0.9, b = -0.6013, c = 2.0, d = 0.5;
  //
  // DynSys<double> tinkerbell(2);
  //
  // tinkerbell.set_state({-0.72, -0.64});
  //
  // Vector<std::function<double(Vector<double>)>> S(2);
  //
  // S[0] = [a, b, c, d](Vector<double> x) -> double
  // { return (x[0]*x[0] - x[1]*x[1] + a*x[0] + b*x[1]); };
  //
  // S[1] = [a, b, c, d](Vector<double> x) -> double
  //   { return (2*x[0]*x[1] + c*x[0] + d*x[1]); };
  //
  // tinkerbell.set_system(S);

  // Lorenz attractor
  double sig = 10, rho = 28, beta = 8.0/3.0;

  DynSys<double> sys(3);

  sys.set_state({1, 1, 1});

  Vector<std::function<double(Vector<double>)>> S(3);

  S[0] = [sig, rho, beta](Vector<double> x) -> double
  { return (sig*(x[1] - x[0])); };

  S[1] = [sig, rho, beta](Vector<double> x) -> double
  { return (x[0]*(rho-x[2]) - x[1]); };

  S[2] = [sig, rho, beta](Vector<double> x) -> double
  { return (x[0]*x[1] - beta*x[2]); };

  sys.set_system(S);

  sys.print();

  std::FILE* f = std::fopen("./dat/system.dat", "w");

  Timer<double> timer;

  timer.start();

  for (unint i = 0; i < 1<<18; i++) {
    sys.rk4(0.001);
    // sys.print();
    std::fprintf(f, "%f\t%f\t%f\n", sys.get_state(0), sys.get_state(1), sys.get_state(2));
  }

  timer.stop();
  std::cout << "C style: " << timer.val() << '\n';

  sys.print();

  Gnuplot gp;
  gp("splot './dat/system.dat' w l ls 1");

  return 0;

}
