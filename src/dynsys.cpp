#include "dynsys.h"

/**************************/
/*    Dynamical System    */
/**************************/

// Constructor
template <typename real>
DynSys<real>::DynSys (unint N)
  : _N(N), _t(0.0), _p(0),
  _x0(Vector<real>(N)), _x1(Vector<real>(N)),
  _S(Vector<std::function<real(Vector<real>)>>(N))
{}

// Set system state
template <typename real>
void DynSys<real>::set_state(unint n, real xn)
{
  if (_p)
    _x1[n] = xn;
  else
    _x0[n] = xn;
}

template <typename real>
void DynSys<real>::set_state(Vector<real> x)
{
  if (_p)
    _x1 = x;
  else
    _x0 = x;
}

// Set function in the system
template <typename real>
void DynSys<real>::set_function(unint n, std::function<real(Vector<real>)> f)
{ _S[n] = f; }

// Set system
template <typename real>
void DynSys<real>::set_system(Vector<std::function<real(Vector<real>)>> S)
{ _S = S; }

// Map evolve (Discrete-time)
template <typename real>
void DynSys<real>::map_evolve(void)
{
  if (_p)
    for (unint k = 0; k < _N; k++)
      _x0[k] = _S[k](_x1);
  else
    for (unint k = 0; k < _N; k++)
      _x1[k] = _S[k](_x0);

  _p = !_p;
  _t++;
}

// Euler integration
template <typename real>
void DynSys<real>::euler(real dt)
{
  if (_p)
    for (unint k = 0; k < _N; k++)
      _x0[k] = dt*_S[k](_x1);
  else
    for (unint k = 0; k < _N; k++)
      _x1[k] = dt*_S[k](_x0);

  _p = !_p;

  _t += dt;
}

template <typename real>
void DynSys<real>::euler(unint steps, real dt)
{
  for (unint i = 0; i < steps; i++) {
    this->euler(dt);
  }
}

// Runge-Kutta 4th order
template <typename real>
void DynSys<real>::rk4(real dt)
{
  Vector<real> k1(_N), k2(_N), k3(_N), k4(_N);
  Vector<real> v1(_N), v2(_N), v3(_N);

  if (_p)
  {
    for (unint i = 0; i < _N; i++)
    {
      k1[i] = dt*_S[i](_x1);
      v1[i] = _x1[i] + k1[i]/2.0;
    }
    for (unint i = 0; i < _N; i++)
    {
      k2[i] = dt*_S[i](v1);
      v2[i] = _x1[i] + k2[i]/2.0;
    }
    for (unint i = 0; i < _N; i++)
    {
      k3[i] = dt*_S[i](v2);
      v3[i] = _x1[i] + k3[i];
    }
    for (unint i = 0; i < _N; i++)
    {
      k4[i] = dt*_S[i](v3);
      _x0[i] = _x1[i] + (k1[i] + 2*k2[i] + 2*k3[i] + k4[i])/6.0;
    }
  }
  else
  {
    for (unint i = 0; i < _N; i++)
    {
      k1[i] = dt*_S[i](_x0);
      v1[i] = _x0[i] + k1[i]/2.0;
    }
    for (unint i = 0; i < _N; i++)
    {
      k2[i] = dt*_S[i](v1);
      v2[i] = _x0[i] + k2[i]/2.0;
    }
    for (unint i = 0; i < _N; i++)
    {
      k3[i] = dt*_S[i](v2);
      v3[i] = _x0[i] + k3[i];
    }
    for (unint i = 0; i < _N; i++)
    {
      k4[i] = dt*_S[i](v3);
      _x1[i] = _x0[i] + (k1[i] + 2*k2[i] + 2*k3[i] + k4[i])/6.0;
    }
  }

  _p = !_p;

  _t += dt;
}

// Get state
template <typename real>
real DynSys<real>::get_state(unint n)
{
  if (_p)
    return _x1[n];
  else
    return _x0[n];
}

// Print
template <typename real>
void DynSys<real>::print(void)
{
  std::cout << " t = " << _t << "\t:\t";
  if (_p)
    for (unint i = 0; i < _N; i++)
      std::cout << _x1[i] << '\t';
  else
    for (unint i = 0; i < _N; i++)
      std::cout << _x0[i] << '\t';

  std::cout << std::endl;
}

// Explicit instatiations
INSTANTIATE_REAL(DynSys)
