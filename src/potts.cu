#include "potts.cuh"

/********************/
/*    Potts Model   */
/********************/

#include <fstream>
#include <cooperative_groups.h>

#include "gnuplot.h"
#include "timer.h"

namespace cg = cooperative_groups;

// #define DEBUG_POTTS

#define LTHR 32
#define MAXBLOCK_X 1024
#define MAXGRID_X 2147483647
#define MAXGRID_Y 65535

/*****************************/
/*    CUDA General Kernels   */
/*****************************/

// Device reduce sum
// TODO: Implement Device reduce sum in every sum
template <typename T>
__device__ T reduce_sum(T in, cg::thread_block cta)
{
    extern __shared__ T sdata[];

    unint ltid = threadIdx.x;

    sdata[ltid] = in;

    cg::sync(cta);

    // Do reduction in shared mem
    for (unint s = blockDim.x / 2 ; s > 0 ; s >>= 1)
    {
        if (ltid < s)
        {
            sdata[ltid] += sdata[ltid + s];
        }

        cg::sync(cta);
    }

    return sdata[0];
}

// RNG init kernel
__global__ void initRNG(curandState *const rngStates, const unint seed)
{
    // Determine thread ID
    unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
    // Initialise the RNG
    curand_init(seed, tid, 0, &rngStates[tid]);
}

// Shared memory reduction
template <typename T>
__global__ void red_shmem(const unint N, T *in_data, T *out_data)
{
  // Shared memory
  __shared__ T sdata[MAXBLOCK_X];

  // Thread ID
  unint tid = blockIdx.x*blockDim.x + threadIdx.x;
  unint offset = blockDim.x*gridDim.x;

  // Local thread ID
  unint ltid = threadIdx.x;

  // Fill shared memory
  T temp = 0;
  for (unint i = tid; i < N; i += offset){
    temp += in_data[tid];
  }
  sdata[ltid] = temp;
  __syncthreads();

  // Reduce loop
  unint step = blockDim.x/2;
  while (step != 0)
  {
    if (ltid < step)
      sdata[ltid] += sdata[ltid + step];
    __syncthreads();
    step /= 2;
  }

  // Write data to output
  if (ltid == 0)
    out_data[blockIdx.x] = sdata[0];

}

// TODO: Consecutive reduce sums for very large arrays

// Square every elemente in the array
template <typename T>
__global__ void sq_arr(const unint N, T *const arr, T *const arr2)
{
  // Thread indices
  unint tid = blockIdx.x*blockDim.x + threadIdx.x;
  unint toff = gridDim.x*blockDim.x;

  for (unint i = tid; i < N; i += toff)
    arr2[i] = arr[i]*arr[i];
}

/*****************************/
/*    CUDA Potts2D Kernels   */
/*****************************/

// Initialize lattice with a given value
__global__ void Lattice_T0zero_fill(const unint N, const unint s0, unint *const lattice)
{
  unint tid = blockIdx.x*blockDim.x + threadIdx.x;
  unint toff = blockDim.x*gridDim.x;

  for (unint i = tid; i < N; i += toff)
    lattice[i] = s0;
}

// Initialize lattice with infinite temperature
__global__ void Lattice_T0inf_fill(const unint N, const unint q, unint *const lattice, curandState *const rngStates)
{
  unint tid = blockIdx.x*blockDim.x + threadIdx.x;
  unint toff = blockDim.x*gridDim.x;

  for (unint i = tid; i < N; i += toff)
    lattice[i] = (unint)q*curand_uniform(&rngStates[i]);

}

// Magnetization
__global__ void Magnet(const unint N, const unint *const lattice, unint *const sum_red)
{
  // Thread indices
  unint tid = blockIdx.x*blockDim.x + threadIdx.x;
  unint toff = gridDim.x*blockDim.x;
  // Thread group
  cg::thread_block cta = cg::this_thread_block();

  unint temp = 0;

  // Accumulate
  for (size_t i = tid; i < N; i += toff)
    if (lattice[i] == 1) temp++;

  // Reduce sum
  temp = reduce_sum<unint>(temp, cta);

  // Store the result
  if (threadIdx.x == 0)
  {
      sum_red[blockIdx.x] = temp;
  }

}

// Calculate interactions between all spins
template <typename real>
__global__ void H_LongRange_int(unint lx, unint ly, unint N, real ssign, unint *lattice, real *E_int)
{
  // Global thread indices
  unint i = blockIdx.x*blockDim.x + threadIdx.x;
  unint j = blockIdx.y*blockDim.y + threadIdx.y;
  // Thread block offset
  unint ioff = gridDim.x*blockDim.x;
  unint joff = gridDim.y*blockDim.y;

  // Lattice coordinates
  unint ix, iy;
  unint jx, jy;

  unint k;

  while (i < N)
  {
    while ((j > i) && (j < N))
    {
      if (lattice[j] == lattice[i]) {
        iy = i/lx;
        ix = i%lx;
        jy = j/lx;
        jx = j%lx;
        k = i*N - (i*(i+3))/2 + j - 1;
        E_int[k] = 1.0/powf((jy-iy)*(jy-iy)+(jx-ix)*(jx-ix), ssign);
      }

      j += joff;
    }
    i += ioff;
  }
}

// Calculate all interactions with a given spin
template <typename real>
__global__ void H_LongRange_int_local(unint *lattice, const unint lx, const unint N, const real ssign, const unint p, const unint px, const unint py, const unint s, const unint s_, real *E_int){

  unint k = blockIdx.x*blockDim.x + threadIdx.x;
  // unint koff = blockDim.x*gridDim.x;

  // Lattice index
  unint j;
  // Lattice coordinates
  unint jx, jy;

  // Calculate previous energy (and flip sign)
  if (k < N-1) {
    // Skip site p
    if (k < p) j = k;
    else j = k+1;
    // Calculate interaction
    if(lattice[j] == s){
      jy = j/lx;
      jx = j%lx;
      E_int[k] = 1.0/powf((jy-py)*(jy-py)+(jx-px)*(jx-px), ssign);
    }
    else E_int[k] = 0.0;
  }
  // Calculate new energy
  else if (k < 2*(N-1)) {
    // Set site
    j = k - (N-1);
    // Skip site p
    if (j >= p) j = j + 1;
    // Calculate interaction
    if(lattice[j] == s_){
      jy = j/lx;
      jx = j%lx;
      E_int[k] = -1.0/powf((jy-py)*(jy-py)+(jx-px)*(jx-px), ssign);
    }
    else E_int[k] = 0.0;
  }
}

/*****************************/
/*    GPU Member Functions   */
/*****************************/

// Initialize Lattice
template <typename real>
void Potts2D<real>::_Lattice_fill(const bool T0inf, const unint s0)
{

  unint blockSize = MAXBLOCK_X;
  unint numBlocks = (_N+blockSize-1)/blockSize;
  #ifdef DEBUG_POTTS
  std::cout << "\nLattice fill" << std::endl;
  std::cout << "Thread Blocks =  " << numBlocks << std::endl;
  std::cout << "Block size = " << blockSize << std::endl;
  if (s0 >= _q)
  {
    std::cerr << "ERROR: Initial state invalid." << '\n';
    return;
  }
  #endif

  // Initial temperature
  if (T0inf)
  {
    // Initialize PRNG
    curandState *curands_spin;
    cudaMalloc((void**)&curands_spin, _N*sizeof(curandState));
    initRNG<<<numBlocks, blockSize>>>(curands_spin, 1234);
    Lattice_T0inf_fill<<<numBlocks, blockSize>>>(_N, _q, _gpu_lattice, curands_spin);
    cudaDeviceSynchronize();
    cudaFree(curands_spin);
  }
  else
  {
    Lattice_T0zero_fill<<<numBlocks, blockSize>>>(_N, s0, _gpu_lattice);
    cudaDeviceSynchronize();
  }

  // Retrieve lattice from GPU
  cudaMemcpy(_lattice, _gpu_lattice, _N*sizeof(unint), cudaMemcpyDeviceToHost);

}

// Calculate magnetization
template <typename real>
real Potts2D<real>::_Magnet(void)
{
  const unint blockSize = MAXBLOCK_X;
  unint numBlocks = (_N+blockSize-1)/blockSize;
  size_t shMem = blockSize*sizeof(unint);

  #ifdef DEBUG_POTTS
  std::cout << "\nKERNEL: Magnetization ";
  printf("(%d -> %d)\n", _N, numBlocks);
  std::cout << "Thread Blocks =  " << numBlocks << std::endl;
  std::cout << "Block size = " << blockSize << std::endl;
  std::cout << "Shared Memory = " << shMem << " bytes" << std::endl;
  #endif

  // Reduced arrays
  unint *gpu_sum_red, *sum_red;
  cudaMalloc((void**)&gpu_sum_red, numBlocks*sizeof(unint));
  cudaMemset(gpu_sum_red, 0, numBlocks*sizeof(unint));
  sum_red = new unint[numBlocks];

  // Reduce sum
  Magnet<<<numBlocks, blockSize, shMem>>>(_N, _gpu_lattice, gpu_sum_red);
  cudaDeviceSynchronize();
  cudaMemcpy(sum_red, gpu_sum_red, numBlocks*sizeof(unint), cudaMemcpyDeviceToHost);
  cudaFree(gpu_sum_red);

  // Constants
  real a = (real)_q/(_N*(_q-1.0)), b = 1.0/((real)_q-1.0);
  unint n1 = 0;
  for (size_t i = 0; i < numBlocks; i++){
    // std::cout << sum_red[i] << '\n';
    n1 += sum_red[i];
  }
  delete[] sum_red;

  return (a*n1-b);

}

// Long range Hamiltonian (in units of J)
template <typename real>
real Potts2D<real>::_H_LongRange(void)
{
  // Number of interactions
  unint M = (_N*(_N-1))/2;

  // 2D thread blocks
  // TODO: Set limits to block dimensions
  const unint lthreads = LTHR;
  unint iblocks = (_N+lthreads-1)/lthreads;
  unint jblocks = (_N+lthreads-1)/lthreads;
  dim3 block2D(lthreads, lthreads);
  dim3 grid2D(iblocks, jblocks);

  // Array with energy of interactions
  real *gpu_E_int;
  cudaMalloc((void**)&gpu_E_int, M*sizeof(real));
  cudaMemset(gpu_E_int, 0, M*sizeof(real));

  // Calculate interactions
  #ifdef DEBUG_POTTS
  std::cout << "\nKERNEL: H_LongRange_int" << std::endl;
  printf("Thread Blocks = %dx%d\n", grid2D.x, grid2D.y);
  printf("Block size = %dx%d\n", block2D.x, block2D.y);
  #endif
  H_LongRange_int<<<grid2D, block2D>>>(_lx, _ly, _N, _ssig, _gpu_lattice, gpu_E_int);
  cudaDeviceSynchronize();

  // Thread blocks for reduce sum
  const unint blockSize = MAXBLOCK_X;
  unint numBlocks = (M+blockSize-1)/blockSize;

  #ifdef DEBUG_POTTS
  std::cout << "\nH_LongRange" << std::endl;
  std::cout << "KERNEL: Reduce sum ";
  printf("(%d -> %d)\n", M, numBlocks);
  std::cout << "Thread Blocks =  " << numBlocks << std::endl;
  std::cout << "Block size = " << blockSize << std::endl;
  #endif

  // Array with reduced sum
  real *gpu_E_int_red;
  cudaMalloc((void**)&gpu_E_int_red, numBlocks*sizeof(real));
  cudaMemset(gpu_E_int_red, 0, numBlocks*sizeof(real));

  // Reduce sum
  red_shmem<real><<<numBlocks, blockSize>>>(M, gpu_E_int, gpu_E_int_red);
  cudaDeviceSynchronize();

  // Retrieve data
  cudaFree(gpu_E_int);
  real *E_int_red;
  E_int_red = (real*)malloc(numBlocks*sizeof(real));
  cudaMemcpy(E_int_red, gpu_E_int_red, numBlocks*sizeof(real), cudaMemcpyDeviceToHost);
  cudaFree(gpu_E_int_red);

  // Sum energy
  real E = 0;
  for (size_t i = 0; i < numBlocks; i++)
  {
    E += E_int_red[i];
  }

  free(E_int_red);

  return -E;

}

// Specific heat
template <typename real>
real Potts2D<real>::spec_heat(const unint eq_steps)
{
  #ifdef DEBUG_POTTS
  if (eq_steps >= _t){
    std::cerr << "ERROR: Not enough data for specific heat calculation" << '\n';
    return 0;
  }
  #endif

  // Number of steps to make the calculation
  unint steps = _t - eq_steps;

  unint blockSize = MAXBLOCK_X;
  unint numBlocks = (steps+blockSize-1)/blockSize;

  #ifdef DEBUG_POTTS
  std::cout << "\nSpecific heat calculation" << std::endl;
  std::cout << "KERNEL: sq_arr, red_shmem" << std::endl;
  std::cout << "Thread Blocks =  " << numBlocks << std::endl;
  std::cout << "Block size = " << blockSize << std::endl;
  #endif

  // Array to hold values of energy
  real *gpu_E, *gpu_E2;
  cudaMalloc((void**)&gpu_E, steps*sizeof(real));
  cudaMalloc((void**)&gpu_E2, steps*sizeof(real));

  // Copy energy values to first array
  cudaMemcpy(gpu_E, _E+eq_steps, steps*sizeof(real), cudaMemcpyHostToDevice);

  // Square values and put in another array
  sq_arr<real><<<numBlocks, blockSize>>>(steps, gpu_E, gpu_E2);
  cudaDeviceSynchronize();

  // Reduce Sum
  real *gpu_E_red, *gpu_E2_red;
  cudaMalloc((void**)&gpu_E_red, numBlocks*sizeof(real));
  cudaMalloc((void**)&gpu_E2_red, numBlocks*sizeof(real));

  red_shmem<real><<<numBlocks, blockSize>>>(steps, gpu_E, gpu_E_red);
  cudaDeviceSynchronize();
  red_shmem<real><<<numBlocks, blockSize>>>(steps, gpu_E2, gpu_E2_red);
  cudaDeviceSynchronize();

  cudaFree(gpu_E);
  cudaFree(gpu_E2);

  real *E_red = new real[numBlocks];
  real *E2_red = new real[numBlocks];

  cudaMemcpy(E_red, gpu_E_red, numBlocks*sizeof(real), cudaMemcpyDeviceToHost);
  cudaMemcpy(E2_red, gpu_E2_red, numBlocks*sizeof(real), cudaMemcpyDeviceToHost);

  cudaFree(gpu_E_red);
  cudaFree(gpu_E2_red);

  real E2avg = 0, Eavg = 0;
  for (unint i = 0; i < numBlocks; i++) {
    Eavg += E_red[i];
    E2avg += E2_red[i];
  }
  delete[] E_red;
  delete[] E2_red;

  E2avg = E2avg/steps;
  Eavg = Eavg/steps;

  #ifdef DEBUG_POTTS
  std::cerr << "E2avg = " << E2avg<< '\n';
  std::cerr << "Eavg = " << Eavg<< '\n';
  #endif

  real Eavg2 = Eavg*Eavg;


  return ((E2avg-Eavg2)/(_T*_T));

}

/*****************************/
/*    CPU Member Functions   */
/*****************************/

// Magnetization
template <typename real>
real Potts2D<real>::_Magnet_cpu(void)
{
  // Number of spins si=1
  unint n1 = 0;
  real a = (real)_q/(_N*(_q-1.0)), b = 1.0/((real)_q-1.0);

  for (size_t i = 0; i < _N; i++)
    if (_lattice[i] == 1) n1++;

  return (a*n1-b);
}

// Long range Hamiltonian (in units of J) CPU
template <typename real>
real Potts2D<real>::_H_LongRange_cpu(void)
{
  // Energy
  real E = 0;
  // Distance squared
  real d2;

  // Spins
  size_t i, j;

  //     x , y
  size_t ix, iy;  // Spin i
  size_t jx, jy;  // Spin j

  for (iy = 0; iy < _ly; iy++) {
    for (ix = 0; ix < _lx; ix++) {
      // Spin i
      i = iy*_lx+ix;
      // Interaction with rest of the line
      jy = iy;
      for (jx = ix+1; jx < _lx; jx++) {
        j = jy*_lx+jx;
        if (_lattice[i] == _lattice[j]) {
          d2 = (jx-ix)*(jx-ix);
          E += 1.0/pow(d2, _ssig);
        }
      }
      // Interaction with remaining lines
      for (jy = iy+1; jy < _ly; jy++) {
        for (jx = 0; jx < _lx; jx++) {
          j = jy*_lx+jx;
          if (_lattice[i] == _lattice[j]) {
            d2 = (jy-iy)*(jy-iy) + (jx-ix)*(jx-ix);
            E += 1.0/pow(d2, _ssig);
          }
        }
      }
    }
  }

  return -E;

}

// Long range Hamiltonian local (in units of J) CPU
template <typename real>
real Potts2D<real>::_H_LongRange_local_cpu(unint ix, unint iy, unint s)
{
  real E = 0;
  real d2;

  //     x , y
  size_t jx, jy;  // Spin j

  // Spin j index
  size_t j = 0;

  // Interactions above
  for (jy = 0; jy < iy; jy++) {
    for (jx = 0; jx < _lx; jx++) {
      j = jy*_lx+jx;
      if (_lattice[j] == s) {
        d2 = (jy-iy)*(jy-iy) + (jx-ix)*(jx-ix);
        E += 1.0/pow(d2, _ssig);
      }
    }
  }
  // Interactions with rest of the line
  jy = iy;
  for (jx = 0; jx < ix; jx++) {
    j = jy*_lx+jx;
    if (_lattice[j] == s) {
      d2 = (jx-ix)*(jx-ix);
      E += 1.0/pow(d2, _ssig);
    }
  }
  for (jx = ix+1; jx < _lx; jx++) {
    j = jy*_lx+jx;
    if (_lattice[j] == s) {
      d2 = (jx-ix)*(jx-ix);
      E += 1.0/pow(d2, _ssig);
    }
  }
  // Interactions below
  for (jy = iy+1; jy < _ly; jy++) {
    for (jx = 0; jx < _lx; jx++) {
      j = jy*_lx+jx;
      if (_lattice[j] == s) {
        d2 = (jy-iy)*(jy-iy) + (jx-ix)*(jx-ix);
        E += 1.0/pow(d2, _ssig);
      }
    }
  }

  return -E;

}

// Monte Carlo Metropolis (CPU)
template <typename real>
void Potts2D<real>::MC_Metropolis_cpu(const unint steps)
{

  // PRNG (host)
  std::random_device rand_dev;
  std::mt19937 mt_site(1234);
  std::mt19937 mt_state(5678);
  std::mt19937 mt_metrop(9012);
  // Random position on lattice
  std::uniform_int_distribution<unint> rand_site(0, _N-1);
  // Random state switch
  std::uniform_int_distribution<unint> rand_state(0, _q-2);
  // Random number for Metropolis
  std::uniform_real_distribution<real> rand_metrop(0.0, 1.0);

  // Reset data arrays
  real M = _M[_t];
  real E = _E[_t];
  delete[] _M;
  delete[] _E;
  _M = new real[steps+1];
  _E = new real[steps+1];
  _t = 0;
  _M[0] = M;
  _E[0] = E;

  // Lattice index
  unint p, px, py;
  // Spin state
  unint s, s_;
  // Energy difference
  real dE;
  // // Magnetization constant
  real a = (real)_q/(_N*(_q-1.0));

  // Monte Carlo loop
  for (unint t = 0; t < steps; t++)
  {
    int dn = 0;
    E = 0;
    for (size_t i = 0; i < _N; i++) {
      // Select random site
      p = rand_site(mt_site);
      s = _lattice[p];
      px = p%_lx;
      py = p/_lx;

      // Select random new state
      s_ = rand_state(mt_state);
      if (s_ >= s) s_++;

      // Calculate energy difference
      dE = _H_LongRange_local_cpu(px, py, s_) - _H_LongRange_local_cpu(px, py, s);

      // Calculate acceptance
      if ((dE <= 0) || (rand_metrop(mt_metrop) <= exp(-_BJ*dE)))
      {
        _lattice[p] = s_;
        if (s == 1) dn -= 1;
        else if (s_ == 1) dn += 1;
        E += dE;
      }
    } // Lattice
    // Save data to array
    // _M[t+1] = _Magnet_cpu();
    // _E[t+1] = _H_LongRange_cpu();
    _M[t+1] = _M[t] + a*dn;
    _E[t+1] = _E[t] + E;
  } // MC

  // Advance time
  _t += steps;
  _runs++;

  // Update GPU Lattice
  cudaMemcpy(_gpu_lattice, _lattice, _N*sizeof(unint), cudaMemcpyHostToDevice);

}

// Monte Carlo Metropolis (CPU)
template <typename real>
void Potts2D<real>::MC_Metropolis_cpu(const unint runs, const unint steps)
{
  // PRNG (host)
  std::random_device rand_dev;
  std::mt19937 mt_site(1234);
  std::mt19937 mt_state(5678);
  std::mt19937 mt_metrop(9012);
  // Random position on lattice
  std::uniform_int_distribution<unint> rand_site(0, _N-1);
  // Random state switch
  std::uniform_int_distribution<unint> rand_state(0, _q-2);
  // Random number for Metropolis
  std::uniform_real_distribution<real> rand_metrop(0.0, 1.0);

  // Reset data arrays
  real M = _M[_t];
  real E = _E[_t];
  delete[] _M;
  delete[] _E;
  _M = new real[steps+1];
  _E = new real[steps+1];
  _t = 0;
  _M[0] = M;
  _E[0] = E;
  for (size_t t = 1; t < steps+1; t++) {
    _M[t] = 0;
    _E[t] = 0;
  }

  // Lattice index
  unint p, px, py;
  // Spin state
  unint s, s_;
  // Energy difference
  real dE;
  // // Magnetization constant
  real a = (real)_q/(_N*(_q-1.0));

  for (unint run = 0; run < runs; run++) {
    std::cout << "RUN " << run+1 << '\n';
    // Reset Lattice
    _Lattice_fill(false, 1);
    real M_old = _Magnet_cpu();
    real E_old = _H_LongRange_cpu();
    // Monte Carlo loop
    for (unint t = 0; t < steps; t++)
    {
      int dn = 0;
      E = 0;
      for (size_t i = 0; i < _N; i++) {
        // Select random site
        p = rand_site(mt_site);
        s = _lattice[p];
        px = p%_lx;
        py = p/_lx;

        // Select random new state
        s_ = rand_state(mt_state);
        if (s_ >= s) s_++;

        // Calculate energy difference
        dE = _H_LongRange_local_cpu(px, py, s_) - _H_LongRange_local_cpu(px, py, s);

        // Calculate acceptance
        if ((dE <= 0) || (rand_metrop(mt_metrop) <= exp(-_BJ*dE)))
        {
          _lattice[p] = s_;
          if (s == 1) dn -= 1;
          else if (s_ == 1) dn += 1;
          E += dE;
        }
      } // Lattice
      // Accumulate
      // _M[t+1] += _Magnet_cpu();
      // _E[t+1] += _H_LongRange_cpu();
      M = M_old + a*dn;
      E = E_old + E;
      _M[t+1] += M;
      _E[t+1] += E;
      M_old = M;
      E_old = E;
    } // MC
  } // Runs

  // Normalize data
  for (size_t t = 1; t < steps+1; t++)
  {
    _M[t] /= runs;
    _E[t] /= runs;
  }

  // Advance time
  _t += steps;
  _runs += runs;

  // Update GPU Lattice
  cudaMemcpy(_gpu_lattice, _lattice, _N*sizeof(unint), cudaMemcpyHostToDevice);
}

// Magnetic Susceptibility
template <typename real>
real Potts2D<real>::suscept_cpu(const unint eq_steps)
{
  #ifdef DEBUG_POTTS
  if (eq_steps >= _t){
    std::cerr << "ERROR: Not enough data for specific heat calculation" << '\n';
    return 0;
  }
  #endif

  // Number of steps to make the calculation
  unint steps = _t - eq_steps;

  real Mavg = 0, M2avg = 0;
  for (unint i = 1; i <= steps; i++) {
    real M = _M[eq_steps+i];
    Mavg += M/steps;
    M2avg += M*M/steps;
  }

  real X = (M2avg-Mavg*Mavg)/(_T);

  return X;

}

// Specific heat
template <typename real>
real Potts2D<real>::spec_heat_cpu(const unint eq_steps)
{
  #ifdef DEBUG_POTTS
  if (eq_steps >= _t){
    std::cerr << "ERROR: Not enough data for specific heat calculation" << '\n';
    return 0;
  }
  #endif

  // Number of steps to make the calculation
  unint steps = _t - eq_steps;

  real Eavg = 0, E2avg = 0;
  for (unint i = 1; i <= steps; i++) {
    real E = _E[eq_steps+i]/_N;
    Eavg += E/steps;
    E2avg += E*E/steps;
  }

  real c = (E2avg-Eavg*Eavg)/(_T*_T);

  return c;

}

/*****************************************/
/*    Potts2D General Member Functions   */
/*****************************************/

// Constructor
template <typename real>
Potts2D<real>::Potts2D(const unint lx, const unint ly, const unint q, const real T, const real J, const real sig, const bool T0inf, const unint s0)
  : _lx(lx), _ly(ly), _N(lx*ly), _q(q), _t(0), _runs(0),
    _T(T), _J(J), _BJ(J/T),
    _sig(sig), _ssig((2.0+sig)/2.0)
{

  // Allocate Lattice
  _lattice = new unint[_N];
  cudaMalloc((void**)&_gpu_lattice, _N*sizeof(unint));

  // Fill lattice
  _Lattice_fill(T0inf, s0);

  // Initial data
  _M = new real;
  _E = new real;
  _M[0] = _Magnet();
  _E[0] =  _H_LongRange();

}

// Destructor
template <typename real>
Potts2D<real>::~Potts2D()
{
  // Free memory
  // free(_lattice);
  delete[] _lattice;
  cudaFree(_gpu_lattice);
  delete[] _M;
  delete[] _E;
}

// Reset
template <typename real>
void Potts2D<real>::reset(const real T, const real J, const real sig, const bool T0inf, const unint s0){

  // Reset Lattice
  _Lattice_fill(T0inf, s0);

  // Set parameters
  _T = T;
  _J = J;
  _BJ = J/T;
  _sig = sig;
  _ssig = (2.0+sig)/2.0;

  _runs = 0;

  // Recalculate energy and magnetization
  if (_t != 0)
  {
    delete[] _M;
    delete[] _E;
    _M = new real;
    _E = new real;
    _t = 0;
  }
  _M[0] = _Magnet();
  _E[0] =  _H_LongRange();

}

// Get energy
template <typename real>
real Potts2D<real>::energy(void)
{ return _E[_t]*_J; }

// Get magnetization
template <typename real>
real Potts2D<real>::magnet(void)
{ return _M[_t]; }

// Print Grid
template <typename real>
void Potts2D<real>::print_grid()
{
  std::cout << std::endl;
  for (size_t i = 0; i < _N; i++) {
    if (i%_lx == 0) std::cout << std::endl;
    std::cout << '(' <<_lattice[i] << ')';
  }
  std::cout << "\n\n";
}

// Print
template <typename real>
void Potts2D<real>::print()
{
  std::cout << "\nPotts 2D (CUDA): " << "t = " << _t << "\t" << _q << " states, " << _lx << "x" << _ly << " = " << _N << " lattice" << std::endl;
  std::cout << "T = " << _T << ", J = " <<  _J << ", sigma = " << _sig << std::endl;
  std::cout << "M = " << magnet() << std::endl;
  std::cout << "E = " << energy() << "\te = " << energy()/_N << std::endl;

  #ifdef DEBUG_POTTS
    Timer<double> timer;
    real M_cpu;
    real E_cpu;

    timer.start();
    M_cpu = _Magnet_cpu();
    timer.stop();
    std::cout << "M_cpu = " << M_cpu << std::endl;
    timer.print("Magnet_cpu");

    timer.start();
    E_cpu = _H_LongRange_cpu();
    timer.stop();
    std::cout << "E_cpu = " << E_cpu << "\te_cpu = " << E_cpu/_N << std::endl;
    timer.print("Energy_cpu");
  #endif
}

// Plot
template <typename real>
void Potts2D<real>::plot_magnet(void)
{
  Gnuplot gp;
  gp("set title \'Potts2D\'");
  gp("set xlabel \'MCsteps\'");
  gp("set ylabel \'Magnetization\'");
  gp("plot \'-\' w l");

  // std::cout << "Magnetization data:" << '\n';
  for (unint t = 0; t <= _t; t++) {
    // std::cout << t << '\t' << _M[t] << '\n';
    gp.point2D((real)t, (real)_M[t]);
  }

  gp("e\n");
}

template <typename real>
void Potts2D<real>::plot_energy(void)
{
  Gnuplot gp;
  gp("set title \'Potts2D\'");
  gp("set xlabel \'MCsteps\'");
  gp("set ylabel \'Energy\'");
  gp("plot \'-\' w l");

  // std::cout << "Energy data:" << '\n';
  for (unint t = 0; t <= _t; t++) {
    // std::cout << t << '\t' << _E[t]/_N << '\n';
    gp.point2D((real)t, (real)_E[t]/_N);
  }

  gp("e\n");
}

// Save Energy and Magnetization data
template <typename real>
void Potts2D<real>::write_data(void)
{
  std::ofstream Efile;
  Efile.open("./dat/Potts2D/Potts2D_"+std::to_string(_ly)+'x'+std::to_string(_lx)+"_q="+std::to_string(_q)+"_J="+std::to_string(_J)+"_sig="+std::to_string(_sig)+"_T="+std::to_string(_T)+"_runs="+std::to_string(_runs)+"_t="+std::to_string(_t)+"_Energy.dat");
  for (size_t t = 0; t <= _t; t++)
    Efile << t << '\t' << _E[t]/_N << '\n';
  Efile.close();
  std::ofstream Mfile;
  Mfile.open("./dat/Potts2D/Potts2D_"+std::to_string(_ly)+'x'+std::to_string(_lx)+"_q="+std::to_string(_q)+"_J="+std::to_string(_J)+"_sig="+std::to_string(_sig)+"_T="+std::to_string(_T)+"_runs="+std::to_string(_runs)+"_t="+std::to_string(_t)+"_Magnet.dat");
  for (size_t t = 0; t <= _t; t++)
    Mfile << t << '\t' << _M[t] << '\n';
  Mfile.close();
}

// Template explicit instantiation
template class Potts2D<float>;
template class Potts2D<double>;
