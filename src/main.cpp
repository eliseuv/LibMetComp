#include <iostream>
#include <cmath>
#include <cstdio>
#include <vector>

#include "metcomp.h"
#include "timer.h"
#include "metcomp.cuh"
#include "potts.cuh"

int main() {

  // Get info on GPUs
  CUDA_queryDevices();

  Timer<double> timer;

  // Set system parameters
  unint L = 1<<5, q = 2, s0 = 1;
  float T = 1.0, J = 1.0, sig = 10.0;
  bool T0inf = false;

  // // Create system
  // timer.start();
  // Potts2D<double> sys(L, L, q, T, J, sig, T0inf, s0);
  // timer.stop();
  // timer.print("\nConstructor");
  //
  // sys.print();
  // sys.print_grid();
  //
  // // Monte Carlo
  // timer.start();
  // sys.MC_Metropolis_cpu(1<<9);
  // timer.stop();
  // timer.print("\nMetropolis");
  //
  // sys.print();
  // sys.print_grid();
  //
  // // // Save to File
  // // sys.write_data();
  //
  // // Plot
  // sys.plot_magnet();
  // sys.plot_energy();

  // Magnetic Susceptibility
  timer.start();
  unint eq_steps = 1<<10;
  unint steps = 1<<12;
  for (unint i = 1; i <= 20; i++) {
    T = 0.5 + 0.1*i;
    Potts2D<double> sys(L, L, q, T, J, sig, T0inf, s0);
    sys.MC_Metropolis_cpu(eq_steps+steps);
    double X = sys.suscept_cpu(eq_steps);
    std::cout << T << '\t' << X << '\n';
  }
  timer.stop();
  timer.print("\nMagnetic Susceptibility");

  // // Specific heat
  // timer.start();
  // unint eq_steps = 1<<8;
  // unint steps = 1<<12;
  // for (unint i = 0; i <= 20; i++) {
  //   T = 1.5 + 0.2*i;
  //   Potts2D<double> sys(L, L, q, T, J, sig, T0inf, s0);
  //   sys.MC_Metropolis_cpu(eq_steps+steps);
  //   double c = sys.spec_heat(eq_steps);
  //   std::cout << T << '\t' << c << '\n';
  // }
  // timer.stop();
  // timer.print("\nSpecific heat");


  return 0;

}
