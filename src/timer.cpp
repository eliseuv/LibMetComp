#include "timer.h"

/***************/
/*    Timer    */
/***************/

// Constructor

template <typename real>
Timer<real>::Timer(void)
{
  // Calculate delay
  _start = std::chrono::high_resolution_clock::now();
  _end = std::chrono::high_resolution_clock::now();
  _elapsed = _end - _start;
  _value = _elapsed.count();

  print("Timer delay");

  // Start timer
  _start = std::chrono::high_resolution_clock::now();
}

// Start/Stop
template <typename real>
void Timer<real>::start(void)
{ _start = std::chrono::high_resolution_clock::now(); }

template <typename real>
void Timer<real>::stop(void)
{
  _end = std::chrono::high_resolution_clock::now();
  _elapsed = _end - _start;
  _value = _elapsed.count();
}

// Return elapsed time
template <typename real>
real Timer<real>::val(void)
{ return _value; }

// Print elapsed time
template <typename real>
void Timer<real>::print(std::string description)
{ std::cout << description << " : " << val() << " s" << std::endl; }

// Explicit instatiations
INSTANTIATE_REAL(Timer)
