// Calculate energy diffence for one spin flip
template <typename real>
__global__ void dH_LongRange_local(unint N, unint lx, unint *lattice, real ssig, real *E_int_red, unint t, unint *sites, unint *states)
{
  // Identify thread and block
  cg::thread_block cta = cg::this_thread_block();
  unint tid = blockIdx.x*blockDim.x + threadIdx.x;
  // unint koff = blockDim.x*gridDim.x;

  // Escape unecessary threads
  if (tid > 2*(N-1)) return;

  // Position for each thread
  unint j, jx, jy;

  // Energy of interaction
  real E_int = 0.0;

  // Random values selected (same for every thread)
  // Random site
  unint p = sites[t];
  unint py = p/lx;
  unint px = p%lx;
  unint s = lattice[p];
  // New random state
  unint s_ = sites[t];
  if (s_ >= s) s++;
  // Var for metropolis test
  // real m = metrops[t];
  real m;

  // Select spin for each thread
  // tid = {0 ... N-2}        : present state
  // tid = {N-1 ... 2(N-1)-1} : next state
  if (tid < N - 1)
    j = tid;
  else
    j = tid - (N - 1);
  if (j >= p) j++;
  jy = j/lx;
  jx = j%lx;

  // Present state
  if (tid < N-1)
  {
    if (lattice[j] == s)
      E_int += 1.0/powf((jy-py)*(jy-py)+(jx-px)*(jx-px), ssig);
  }
  // Future state
  else
  {
    if (lattice[j] == s_)
      E_int[tid] += -1.0/powf((jy-py)*(jy-py)+(jx-px)*(jx-px), ssig);
  }

  // Reduce within the block
  E_int = reduce_sum(E_int, cta);

  // Store the result
  if (threadIdx.x == 0)
  {
      E_int_red[blockIdx.x] = E_int;
  }

}

// Monte Carlo Metropolis
template <typename real>
void Potts2D<real>::MC_Metropolis(const unint steps)
{
  // Total number of threads: 2(N-1)
  unint M = 2*(_N-1);

  const unint blockSize = MAXBLOCK_X;
  unint numBlocks = (M + blockSize - 1)/blockSize;

  #ifdef DEBUG_POTTS
  std::cout << "\nKERNERL: MC Metropolis" << std::endl;
  std::cout << "Thread Blocks =  " << numBlocks << std::endl;
  std::cout << "Block size = " << blockSize << std::endl;
  #endif

  // Reset data arrays
  if (_t != 0)
  {
    real Etemp = _E[_t];
    real Mtemp = _M[_t];
    free(_E);
    free(_M);
    _E = (real*)malloc((steps+1)*sizeof(real));
    _M = (real*)malloc((steps+1)*sizeof(real));
    _t = 0;
    _E[0] = Etemp;
    _M[0] = Mtemp;
  }

  // Generate random values
  unint *sites, *states;
  real *metrops;
  sites = (unint*)malloc(steps*sizeof(unint));
  states = (unint*)malloc(steps*sizeof(unint));
  metrops = (real*)malloc(steps*sizeof(real));
  {
    std::random_device rand_dev;
    std::mt19937 mt_site(1234);
    std::mt19937 mt_state(5678);
    std::mt19937 mt_metrop(9012);
    // Random position on lattice
    std::uniform_int_distribution<unint> rand_site(0, _N-1);
    // Random state switch
    std::uniform_int_distribution<unint> rand_state(0, _q-2);
    // Random number for Metropolis
    std::uniform_real_distribution<real> rand_metrop(0.0, 1.0);

    for (unint i = 0; i < steps; i++)
    {
      sites[i] = rand_site(mt_site);
      states[i] = rand_state(mt_state);
      metrops[i] = rand_metrop(mt_metrop);
    }
  }

  // Copy random numbers to GPU
  unint *gpu_sites;
  cudaMalloc((void**)&gpu_sites, steps*sizeof(unint));
  cudaMemset(gpu_sites, 0, steps*sizeof(unint));
  unint *gpu_states;
  cudaMalloc((void**)&gpu_states, steps*sizeof(unint));
  cudaMemset(gpu_states, 0, steps*sizeof(unint));

  cudaMemcpy(gpu_sites, sites, steps*sizeof(unint), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_states, states, steps*sizeof(unint), cudaMemcpyHostToDevice);

  free(sites);
  free(states);


  // Temporary variables for the kernel
  real *gpu_E_int_red, *E_int_red;
  cudaMalloc((void**)&gpu_E_int_red, numBlocks*sizeof(real));
  cudaMemset(gpu_E_int_red, 0, numBlocks*sizeof(real));
  E_int_red = (real*)malloc(numBlocks*sizeof(real));

  for (unint t = 0; t < steps; t++)
  {
    // Calculate Interactions
    dH_LongRange_local<real><<<numBlocks, blockSize>>>(_N, _lx, _gpu_lattice, _ssig, gpu_E_int_red, t, gpu_sites, gpu_states);
    cudaMemcpy(E_int_red, gpu_E_int_red, numBlocks*sizeof(real), cudaMemcpyDeviceToHost);
    real dE = 0.0;
    for (unint i = 0; i < numBlocks; i++)
      dE += E_int_red[i];
    if ((dE <= 0) || (metrops[t] <= exp(-_BJ*dE)))
    {
      // lattice[p] = s_;
      // _E[t+1] = _E[t] + dE;
      // _M[t+1] = _M[t] + s_ - s;
    }
    else
    {
      _E[t+1] = _E[t];
      _M[t+1] = _M[t];
    }
  }

  // Clean up
  cudaFree(gpu_sites);
  cudaFree(gpu_states);
  cudaFree(gpu_E_int_red);
  free(E_int_red);
  free(metrops);

  // Advance time
  _t += steps;

}

// Calculate all interactions with a given spin
// TODO: Monte Carlo process entirely in the GPU
template <typename real>
__global__ void MC_Metropolis_Grid(const unint N, const unint lx, unint *lattice, const real ssig, const real BJ, const unint steps, real *E_int, real *E, unint *M, unint *rand_site, unint *rand_state, real *rand_metrop){

  // Identify thread, block and grid
  unint tid = blockIdx.x*blockDim.x + threadIdx.x;
  // unint koff = blockDim.x*gridDim.x;
  cg::thread_block block_group = cg::this_thread_block();
  // TODO: Grid cooperative group
  // NOTE: Grid synchronization requires compute capability 6.0
  // cg::grid_group grid = cg::this_grid();

  // Escape unecessary threads
  if (tid > 2*(N-1)) return;

  // Vars local to each thread
  unint jf, j, jx, jy;

  // Shared vars
  unint p, px, py, s, s_;
  real dE, m;

  // Select spin for each thread
  // tid = {0 ... N-2}        : present state
  // tid = {N-1 ... 2(N-1)-1} : next state
  if (tid < N - 1)
    jf = tid;
  else
    jf = tid - N + 1;

  // Monte Carlo loop
  for (unint t = 0; t < steps; t++) {

    // Only one thread
    // (or pass array of p and s to the GPU beforehand)
    if (tid == 0)
    {
      // Select random site p
      s = lattice[p];
      py = p/lx;
      px = p%lx;

      // Select random spin s_ (different from lattice[p])

    }
    // grid.sync();

    // Skip p
    if (jf < p)
      j = jf;
    else
      j = jf + 1;
    jy = j/lx;
    jx = j%lx;

    // Present state
    if (tid < N-1)
    {
      if (lattice[j] == s)
        E_int[tid] = 1.0/powf((jy-py)*(jy-py)+(jx-px)*(jx-px), ssig);
      else
        E_int[tid] = 0.0;
    }
    // Future state
    else
    {
      if (lattice[j] == s_)
        E_int[tid] = -1.0/powf((jy-py)*(jy-py)+(jx-px)*(jx-px), ssig);
      else
        E_int[tid] = 0.0;
    }

    // grid.sync();

    // Reduce sum

    // grid.sync();

    // Calculate acceptance (only one thread)
    if (tid == 0)
    {
      // Generate random number m [0.0, 1.0]

      if ((dE <= 0) || (m <= exp(-BJ*dE)))
      {
        lattice[p] = s_;
        E[t+1] = E[t] + dE;
        M[t+1] = M[t] + s_ - s;
      }
      else
      {
        E[t+1] = E[t];
        M[t+1] = M[t];
      }
    }

    // grid.sync();
  }

}

// Set temperature
template <typename real>
void Potts2D<real>::set_temp(const real T, const bool clear)
{

  // Clear vectors
  if ((clear) && (_t != 0))
  {
    _runs = 0;
    real Etemp = _E[_t];
    real Mtemp = _M[_t];
    free(_E);
    free(_M);
    _E = (real*)malloc(sizeof(real));
    _M = (real*)malloc(sizeof(real));
    _t = 0;
    _E[0] = Etemp;
    _M[0] = Mtemp;
  }

  // Set temperature
  _T = T;
  _BJ = _J/T;

}
